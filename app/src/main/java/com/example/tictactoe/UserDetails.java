package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UserDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        Button btn_play = findViewById(R.id.user_btn_play);
        final EditText user1 = findViewById(R.id.user1);
        final EditText user2 = findViewById(R.id.user2);
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserDetails.this, GameActivity.class);
                intent.putExtra("user1", user1.getText().toString());
                intent.putExtra("user2", user2.getText().toString());
                startActivity(intent);
            }
        });
    }
}
