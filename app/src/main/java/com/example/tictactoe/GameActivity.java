package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][]buttons = new Button[3][3];
    private TextView player1 ;
    private TextView player2 ;

    private boolean player1Turn = true;
    private int roundCount =0 ;

    private int player1Points =0;
    private int player2Points =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        player1 = findViewById(R.id.text_view_p1);
        player2 = findViewById(R.id.text_view_p2);
        Intent intent=getIntent();
        String user1 = intent.getStringExtra("user1")+ " points: "+ player1Points;
        String user2 = intent.getStringExtra("user2")+ " points: "+ player2Points;
        player1.setText(user1);
        player2.setText(user2);

        for(int i=0 ; i< 3 ; i++){
            for(int j=0 ; j< 3 ; j++) {
                String buttonId = "btn_" + i + j;
                int resId = getResources().getIdentifier(buttonId,"id",getPackageName());
                buttons[i][j] = findViewById(resId);
                buttons[i][j].setOnClickListener(this);

            }
        }
        Button btnReset = findViewById(R.id.button_reset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBoard();
                player1Points = 0;
                player2Points = 0;
                updatePointsText();
            }
        });
        resetBoard();
    }

    @Override
    public void onClick(View v) {
        if (!(((Button) v).getText().toString().equals(""))){
            return;
        }

        if(player1Turn){
            ((Button)v).setText("X");
        }
        else{
            ((Button)v).setText("O");
        }

        roundCount ++;
        if(checkForWin()){
            if(player1Turn){
                player1Wins();
            }
            else{
                player2Wins();
            }
        }
        else {
            if (roundCount == 9) {
                draw();
            } else {
                player1Turn = !player1Turn;
            }
        }
    }


    public boolean checkForWin(){
        String[][]table = new String[3][3];
        for(int i=0 ; i< 3 ; i++){
            for(int j=0 ; j< 3 ; j++) {
                table[i][j] =buttons[i][j].getText().toString();

            }
        }

        for(int i=0 ; i< 3 ; i++) {
            if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2]) && !table[i][0].equals("")) {
                return true;
            }

            if (table[0][i].equals(table[1][i]) && table[0][i].equals(table[2][i]) && !table[0][i].equals("")) {
                return true;
            }
        }

        if(table[0][0].equals(table[1][1]) && table[0][0].equals(table[2][2])&& !table[0][0].equals("")) {
            return true;
        }

        if(table[0][2].equals(table[1][1]) && table[0][2].equals(table[2][0])&& !table[0][2].equals("")) {
            return true;
        }
        return false;
    }
    private void player1Wins(){
        player1Points++;
        Intent intent=getIntent();
        String user2 = intent.getStringExtra("user1");
        Toast.makeText(this,user2+" wins",Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }
    private void player2Wins(){
        player2Points++;
        Intent intent=getIntent();
        String user2 = intent.getStringExtra("user2");
        Toast.makeText(this,user2+" wins",Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }
    private void draw(){
        Toast.makeText(this,"Draw",Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    public void resetBoard(){
        for(int i=0 ; i< 3 ; i++){
            for(int j=0 ; j< 3 ; j++) {
                buttons[i][j].setText("");

            }
        }
        roundCount=0;
        player1Turn = true;
    }

    public void updatePointsText(){
        Intent intent=getIntent();
        String user1 = intent.getStringExtra("user1")+ " points: "+ player1Points;
        String user2 = intent.getStringExtra("user2")+ " points: "+ player2Points;
        player1.setText(user1);
        player2.setText(user2);
    }

}
